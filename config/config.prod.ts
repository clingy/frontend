import {RuleSetQuery} from "webpack";

const prodFileOpts: RuleSetQuery = {
	name: '[path][name].[ext]',
	publicPath: '/',
	context: './src'
};

export {
	prodFileOpts
}
