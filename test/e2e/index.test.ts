import {Selector} from 'testcafe';

import {should} from 'chai';
should();

fixture`Index Page`.page`../../dist/index.html`;

test('Check if reversing input works', async t => {
	await t
		.typeText('input', 'i am kirinnee');
	
	const value = await Selector(".message").textContent;
	
	value.should.equal("eennirik ma i test2"); 
});