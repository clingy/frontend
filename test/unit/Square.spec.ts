import {should} from 'chai';
import {Dog} from "../../src/classLibrary/Dog";

should();

describe("Dog", () => {
	
	const korgi: Dog = new Dog();
	
	it("should return woof when bark", () => {
		korgi.bark().should.equal("woof");
	});
	
});
