import {Core, Kore} from "@kirinnee/core"; 
import {WebAuth} from "auth0-js"; 
import {AuthService} from "../../classLibrary/auth/AuthService";
import {ApiGateway} from "../../classLibrary/auth/ApiGateway";
import authConfig from "../../../config/auth_config.json";

const core: Core = new Kore();
core.ExtendPrimitives();


declare var PRODUCTION: boolean;
const config: { domain: string, clientId: string } = PRODUCTION ? authConfig.prod : authConfig.dev;

const webAuth: WebAuth = new WebAuth({
    domain: config.domain,
    redirectUri: `${window.location.origin}/callback`,
    clientID: config.clientId,
    responseType: 'id_token',
    scope: 'openid email profile'
});
const auth: AuthService = new AuthService(webAuth);
const host: string = PRODUCTION ? "https://clingy.com" : "http://localhost:3001";
const gateway: ApiGateway = new ApiGateway(host, auth);

const $$ = (i: number): Promise<void> => new Promise<void>(r => setTimeout(r, i));
const isMobile = (): boolean => window.innerHeight > window.innerWidth;
const isPWA = (): boolean => window.matchMedia('(display-mode: standalone)').matches; 

export {
    $$,
    isMobile,
    isPWA, 
    core,
    auth, gateway, 
}
