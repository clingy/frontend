import Vue from 'vue';
import App from './App.vue';
import './index.scss';
import {router} from "./router";
import {images} from './images';
import AuthPlugin from "./plugins/AuthPlugin";
import ApiGatewayPlugin from "./plugins/ApiGatewayPlugin";
import {gateway} from "./init";
import {deferredPrompt} from "./service-worker"


Vue.config.productionTip = false;
Vue.use(AuthPlugin);
Vue.use(ApiGatewayPlugin(gateway));

new Vue({
    router,
    render: h => h(App)
}).$mount('#app');

export {
    images,
    deferredPrompt, 
}
