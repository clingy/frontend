import Vue from 'vue'
import Router, {Route, RouteConfig} from 'vue-router'
import Home from "./components/Home.vue";
import About from "./views/About.vue";
import NotFound from "./views/NotFound.vue";

import Profile from "./views/Profile.vue";
import VerifyEmail from "./views/VerifyEmail.vue";
import Callback from "./components/Callback.vue";
import {auth} from "./init";

Vue.use(Router);

const routes: RouteConfig[] = [
    {path: '/', name: 'home', component: Home, meta: {public: true, private: true}},
    {path: '/about', name: 'about', component: About, meta: {public: true, private: true}},
    {path: '*', name: '404', component: NotFound, meta: {public: true, private: true}},

    {path: '/callback', name: 'callback', component: Callback, meta: {public: true, private: true}},
    {path: '/profile', name: 'profile', component: Profile, meta: {public: false, private: true}},
    {path: '/verify_email', name: 'verify_email', component: VerifyEmail, meta: {public: false, private: true}}
];
const router: Router = new Router({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

//Middle Ware
router.beforeEach(async (to: Route, from: Route, next) => {
    const guard: { public: boolean, private: boolean } = to.meta;
    if (to.path === "/callback") return next();
    if (to.path === "/") {
        try {
            await auth.renewTokens()
        } catch {
        }
        if (auth.isAuthenticated()) {
            if (!auth.profile!.email_verified) {
                if (from.path !== "/verify_email")
                    await router.push("/verify_email");
                return;
            } else {
                if (from.path !== "/profile")
                    await router.push("/profile");
                return;
            }
        } else {
            if (from.path !== "/about")
                await router.push("/about");
            return;
        }
    }
    if (guard.public && !auth.isAuthenticated()) return next();
    if (guard.private && auth.isAuthenticated()) {
        if (!guard.public && !auth.profile!.email_verified && to.path !== "/verify_email") {
            await router.push("/verify_email");
            return;
        } else {
            return next();
        }
    }
    auth.login({target: to.path});
});

export {router};
