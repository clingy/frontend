import {AuthService} from "./AuthService";
import EventEmitter from 'events';
import {ApiError} from "./ApiError";


class ApiGateway extends EventEmitter {
	private readonly host: string;
	private readonly auth: AuthService;
	public readonly eventKey: string = "apiError";
	constructor(host: string, auth: AuthService) {
		super();
		this.host = host;
		this.auth = auth;
	}
	
	protected Endpoint(path: string): string {
		return `${this.host}/${path}`;
	}
	
	protected Reject(name: string, description: string, code: number): Promise<any> {
		const error: ApiError = {
			message: {
				message: description
			},
			name,
			type: code
		};
		this.emit(this.eventKey, error);
		return Promise.reject()
	}
	
	async ping<T>(endpoint: RequestInfo, method: string = "GET", body: any = null, headers: HeadersInit = {
		accept: "application/json",
		'Content-Type': 'application/json'
	}): Promise<T> {
		try {
			return await this.fetch(endpoint, {
				body: body == null ? null : JSON.stringify(body),
				method,
				headers
			});
		} catch (e) {
			this.emit(this.eventKey, e);
			return {} as T;
		}
	}
	
	async fetch<T>(input: RequestInfo, init?: RequestInit): Promise<T> {
		if (this.auth.isAuthenticated()) {
			const authorization: string = `Bearer ${this.auth.idToken}`;
			if (init == null) init = {};
			if (init.headers == null) init.headers = {};
			(init.headers as any)['authorization'] = authorization;
		}
		const response: Response = await fetch(input, init);
		if (response.status === 204) {
			return {} as T;
		}
		const body = await response.json();
		if (!response.ok) return Promise.reject({name: "Error", message: body, type: response.status});
		return body;
	}
}

export {ApiGateway}
